#! /usr/bin/env python3

import rospy

import numpy as np

import apl_msgs.msg


class SBE43VoltageConverter:
    # Constants for the calculation of Oxsol(T,S)
    # Using the Garcia and Gordon 1992 formula
    # As documented on p.158 of the Seabird data processing manual
    A0 = 2.00907
    A1 = 3.22014
    A2 = 4.0501
    A3 = 4.94457
    A4 = -0.256847
    A5 = 3.88767

    B0 = -0.00624523
    B1 = -0.00737614
    B2 = -0.010341
    B3 = -0.00817083

    C0 = -0.000000488682

    def __init__(self) -> None:
        self.setup_parameters()

        self.oxygen_pub = rospy.Publisher(
            "~oxygen", apl_msgs.msg.DissolvedOxygen, queue_size=1
        )

        self.voltage_sub = rospy.Subscriber(
            "~voltage", apl_msgs.msg.Adc, self.voltage_callback
        )

        # We will use the most recent CTD data to calculate oxygen concentration
        # from voltage. This node does not attempt to interpolate.
        # Given how we are using this data, we don't bother with a mutex around it,
        # even if it might be possible for it to update in the middle of an oxygen
        # calculation.
        self.ctd_data = None
        self.ctd_sub = rospy.Subscriber("~ctd", apl_msgs.msg.Ctd, self.ctd_callback)

    def voltage_callback(self, adc_msg: apl_msgs.msg.Adc) -> None:
        # TODO: Implement me!
        if self.ctd_data is None:
            rospy.logwarn_throttle(
                15.0,
                "Have not recieved CTD data yet, so cannot calculate oxygen concentration",
            )
            return
        dt = rospy.Time.now().to_sec() - self.ctd_data.header.stamp.to_sec()
        if dt > self.ctd_expiration_secs:
            rospy.logwarn_throttle(
                1.0,
                "CTD data is {} seconds old; not attempting to calculate oxygen concentration".format(
                    dt
                ),
            )
            return
        oxygen_msg = apl_msgs.msg.DissolvedOxygen()
        oxygen_msg.header = adc_msg.header
        pressure_dbar = self.ctd_data.pressure / 1e4
        oxygen_msg.dissolved_oxygen = self.calc_oxygen(
            adc_msg.voltage,
            self.ctd_data.temperature,
            self.ctd_data.salinity,
            pressure_dbar,
        )
        self.oxygen_pub.publish(oxygen_msg)

    def ctd_callback(self, msg: apl_msgs.msg.Ctd) -> None:
        self.ctd_data = msg

    def setup_parameters(self) -> None:
        self.s_oc = rospy.get_param("~s_oc")
        self.v_offset = rospy.get_param("~v_offset")
        self.tau_20 = rospy.get_param("~tau_20")
        self.a = rospy.get_param("~a")
        self.b = rospy.get_param("~b")
        self.c = rospy.get_param("~c")
        self.e_nominal = rospy.get_param("~e_nominal")
        self.ctd_expiration_secs = rospy.get_param("~ctd_expiration_secs")

    @classmethod
    def calc_oxsol(cls, tt: float, ss: float) -> float:
        """
        Calculate Oxygen Saturation (mL/L) for a given temperature and salinity.

        * tt: Temperature, in degrees C
        * ss: Salinity, in parts-per-thousand (aka PSU)
        """
        rospy.logdebug(
            "Calculating Oxygen Saturation for temp = {} deg C, and salinity = {} PSU".format(
                tt, ss
            )
        )
        ts = np.log((298.15 - tt) / (273.15 + tt))

        oxsol = np.exp(
            (
                cls.A0
                + cls.A1 * ts
                + cls.A2 * ts**2
                + cls.A3 * ts**3
                + cls.A4 * ts**4
                + cls.A5 * ts**5
            )
            + ss * (cls.B0 + cls.B1 * ts + cls.B2 * ts**2 + cls.B3 * ts**3)
            + cls.C0 * ss**2
        )
        rospy.logdebug("Resulting Oxsol = {}".format(oxsol))
        return oxsol

    def calc_tau(self, tt: float, pp: float) -> float:
        """
        Calculate the sensor's time constant
        * tt: temperature, degrees C
        * pp: pressure, dbar (or 1e4 Pascals)
        """
        # TODO: I've asked Craig what the actual formula should be.
        return self.tau_20

    def calc_dvdt(self) -> float:
        # TODO: I've asked Craig what type of filter we should use on this.
        #       For now, ignoring the time dependence on the system.
        return 0.0

    def calc_oxygen(self, vv: float, tt: float, ss: float, pp: float) -> float:
        """
        Calculate oxygen concentration (ml/l) from SBE43's output voltage,
        calibration data, and environmental parameters.
        * tt: temperature, degrees C
        * ss: salinity, ppt (aka PSU)
        * pp: pressure, dbar (or 1e4 Pascals)
        """
        oxsol = self.calc_oxsol(tt, ss)
        tau = self.calc_tau(tt, pp)
        dvdt = self.calc_dvdt()
        kk = tt + 273.15  # degrees kelvin
        oxygen = (
            self.s_oc
            * (vv + self.v_offset + tau * dvdt)
            * oxsol
            * np.exp(self.e_nominal * pp / kk)
            * (1 + self.a * tt + self.b * tt**2 + self.c * tt**3)
        )
        return oxygen

    def run(self):
        rospy.spin()


def main():
    rospy.init_node("sbe43_voltage_converter")

    vc = SBE43VoltageConverter()
    vc.run()
