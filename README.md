## sbe43_voltage_converter

Node that converts voltage readings from a SBE43 dissolved oxygen sensor into
oxygen concentration.

This requires also subscribing to CTD data published by the Remus, so can't be
done as part of the driver itself.

IMPORTANT: This should not be used for final scientific data; it is only intended for real-time sanity checking of the sensor.

Limitations include:
* No interpolation on CTD data; simply uses the most recently received value.
* No correction for sensor dynamics; assumes the time constant is 0.

Tested using spoofed CTD data. In order to get the header timestamps to be correct, rostopic pub requires both `header: auto` and `-s`:
~~~
rostopic pub /ctd apl_msgs/Ctd "header: auto
temperature: 20.0
pressure: 101325
sound_speed: 0.0" -r 5 -s
~~~
